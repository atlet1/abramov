<?php

function fib($n, $pre = 1, $pre_pre = 1){    
    if ($n == 1) return 1;
    if ($n < 1) return false;
    if ($n == 2)
    return $pre;
    
    return fib($n - 1, $pre + $pre_pre, $pre);
}

echo fib(8); //21

echo '<hr>';

function fibArr($n) {
    $fib_array = [1, 1];
    for ($i = 2; $i < $n; $i++) {
        $fib_array[$i] = $fib_array[$i - 1] + $fib_array[$i - 2];
    }
    return array_pop($fib_array);
}

echo fibArr(8); //21
